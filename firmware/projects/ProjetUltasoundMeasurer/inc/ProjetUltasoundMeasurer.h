/*! @mainpage ProjetUltasoundMeasurer *
 * \section genDesc Descripcion General
 *
 *
 * This application makes a measurer of ultrasound with periferic device
 *
 * \section hardConn Hardware Connection
 *
 * | HC-SR4      	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	ECHO	 	| 	T_FIL2		|
 * | 	TRIGGER	 	| 	T_FIL3		|
 * | 	+5V	 	    | 	+5V		    |
 * | 	GND	 	    | 	GND		    |
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 03/09/2021 | Document creation		                         |
 * | 10/09/2021	| creation measurer of ultrasound				 |
 * | 15/09/2021	| finish the measurer of ultrasound				 |		                     |
 *
 * @author Guido Tamburrini
 *
 */

#ifndef _TEMPLATE_H
#define _TEMPLATE_H


/*==================[inclusions]=============================================*/

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


#endif /* #ifndef _BLINKING_H */

