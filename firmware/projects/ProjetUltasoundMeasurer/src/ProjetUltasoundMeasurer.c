/*! @mainpage ProjetUltasoundMeasurer *
 * \section genDesc Descripcion General
 *
 *
 * This application makes a measurer of ultrasound with periferic device
 *
 * \section hardConn Hardware Connection
 *
 * | HC-SR4      	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	ECHO	 	| 	T_FIL2		|
 * | 	TRIGGER	 	| 	T_FIL3		|
 * | 	+5V	 	    | 	+5V		    |
 * | 	GND	 	    | 	GND		    |
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 03/09/2021 | Document creation		                         |
 * | 10/09/2021	| creation measurer of ultrasound				 |
 * | 15/09/2021	| finish the measurer of ultrasound				 |		                     |
 *
 * @author Guido Tamburrini
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/ProjetUltasoundMeasurer.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "gpio.h"
#include "hc_sr4.h"
#include "delay.h"
#include "DisplayITS_E0803.h"
/*==================[macros and definitions]=================================*/
/*==================[internal data definition]===============================*/
/*==================[internal functions declaration]=========================*/
void LedDistanceControlCentimetres(int16_t valor_distancia);
void LedDistanceControlPulgadas();
/*==================[external data definition]===============================*/
/*==================[external functions definition]==========================*/

void LedDistanceControlCentimetres(int16_t valor_distancia)
{
	if(valor_distancia<10)
			{
				GPIOOn(LED_RGB_B);
				LedOff(LED_1);
				LedOff(LED_2);
				LedOff(LED_3);
			};
			if(10<=valor_distancia && valor_distancia<20)
			{
				GPIOOn(LED_RGB_B);
				LedOn(LED_1);
				LedOff(LED_2);
				LedOff(LED_3);
			};
			if(20<=valor_distancia && valor_distancia<30)
			{
				GPIOOn(LED_RGB_B);
				LedOn(LED_1);
				LedOn(LED_2);
				LedOff(LED_3);
			};
			if(30<=valor_distancia && valor_distancia<40)
			{
				GPIOOn(LED_RGB_B);
				LedOn(LED_1);
				LedOn(LED_2);
				LedOn(LED_3);
			};
			if(40<=valor_distancia && valor_distancia<60)
			{
				GPIOOn(LED_RGB_R);
				LedOn(LED_1);
				LedOn(LED_2);
				LedOn(LED_3);
			};
};
////////////////////////////////////

void LedDistanceControlPulgadas(distancia)
{
	//idem que cm pero en pulgadas
};
///////////////////////////////////

int main(void)
{
	SystemClockInit();	//inicial el clock de la CIAA
	LedsInit();
	SwitchesInit();

	uint8_t teclas = 0;
	uint32_t sec = 3;
	int16_t distancia = 0;
	int16_t tiempo_de_distancia = 0;

	gpio_t pins_configs[7] = {GPIO_LCD_1, GPIO_LCD_2, GPIO_LCD_3, GPIO_LCD_4, GPIO_1, GPIO_3, GPIO_5};
	uint8_t prender = 0;
	uint8_t hold = 1;

	//Puntero a vector
	gpio_t *ptr_pins_configs;
	ptr_pins_configs = pins_configs;
	ITSE0803Init(ptr_pins_configs);

	// Declaro las funciones
	HcSr04Init(GPIO_T_FIL2 , GPIO_T_FIL3);
	HcSr04ReadDistance();


	while (1)

	{		DelayMs(500);
			teclas = SwitchesRead();

			switch(teclas)
			{
			 	 case SWITCH_1:
			 		 LedOn(LED_1);
			 		 DelaySec(sec);
			 		 LedOff(LED_1);
			 		 prender =! prender;
			 		 break;

			 	 case SWITCH_2:
			 		 LedOn(LED_2);
			 		 DelaySec(sec);
			 		 LedOff(LED_2);
			 		 hold =! hold;
			 		 break;
			}//fin switch

			if(prender)
			{
				tiempo_de_distancia = HcSr04ReadDistance();
				distancia = HcSr04ReadDistanceCentimeters(tiempo_de_distancia);

				if (hold)
				{
					LedDistanceControlCentimetres(distancia);
					ITSE0803DisplayValue(distancia);
				}
			}//fin primer if
			else
				{
				GPIOOff(LED_RGB_R);
				LedOff(LED_1);
				LedOff(LED_2);
				LedOff(LED_3);
				ITSE0803DisplayValue(0);
				}//fin else
	}//fin while
	return 0;
}//fin main

/*==================[end of file]============================================*/

