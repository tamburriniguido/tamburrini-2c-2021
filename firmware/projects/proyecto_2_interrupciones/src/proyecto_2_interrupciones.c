/*! @mainpage proyecto_2_interrupciones *
 * \section genDesc Descripcion General
 *
 *
 * This application makes a measurer of ultrasound with periferic device
 * for two methods, one for the computer and other with the EDU-CIAA
 *
 * \section hardConn Hardware Connection
 *
 * | HC-SR4      	|   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	ECHO	 	| 	T_FIL2		|
 * | 	TRIGGER	 	| 	T_FIL3		|
 * | 	+5V	 	    | 	+5V		    |
 * | 	GND	 	    | 	GND		    |
 *
 *  | ITS_E0803     |   EDU-CIAA	|
 * |:--------------:|:--------------|
 * | 	LCD1	 	| 	D1		    |
 * | 	LCD2	 	| 	D2		    |
 * | 	LCD3	    | 	D3	        |
 * | 	LCD4	    | 	D4		    |
 * | 	GPIO1	    | 	SEL_0	    |
 * | 	GPIO3	    | 	SEL_1	    |
 * | 	GPIO5	    | 	SEL_2	    |
 * | 	+5V	 	    | 	+5V	        |
 * | 	GND	 	    | 	GND		    |
 *
 *
 *
 * @section changelog Changelog
 *
 * |   Date	    | Description                                    |
 * |:----------:|:-----------------------------------------------|
 * | 03/09/2021 | Document creation		                         |
 * | 10/09/2021	| creation measurer of ultrasound				 |
 * | 15/09/2021	| finish the measurer of ultrasound				 |
 * | 22/09/2021	| modifiation the ultrasound measurer 		 	 |
 * | 24/09/2021	| finish the ultrasound	measurer proyecto_2	 	 |
 * @author Guido Tamburrini
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/proyecto_2_interrupciones.h"       /* <= own header */
#include "systemclock.h"
#include "led.h"
#include "switch.h"
#include "gpio.h"
#include "hc_sr4.h"
#include "timer.h"
#include "delay.h"
#include "DisplayITS_E0803.h"
#include "uart.h"
/*==================[macros and definitions]=================================*/
/*==================[internal data definition]===============================*/
uint32_t sec = 3;
uint8_t prender = 0;
uint8_t hold = 1;
int32_t distancia = 0;//antes era 16
int16_t tiempo_de_distancia = 0;
uint8_t dat;

/*==================[internal functions declaration]=========================*/
void LedDistanceControlCentimetres(int16_t);
void LedDistanceControlPulgadas();
int16_t HcSr04ReadDistanceCentimeters(int16_t );
/*==================[external data definition]===============================*/
/*==================[external functions definition]==========================*/

void LedDistanceControlCentimetres(int16_t valor_distancia)
{
	if(valor_distancia<10)
			{
				GPIOOn(LED_RGB_B);
				LedOff(LED_1);
				LedOff(LED_2);
				LedOff(LED_3);
			};
			if(10<=valor_distancia && valor_distancia<20)
			{
				GPIOOn(LED_RGB_B);
				LedOn(LED_1);
				LedOff(LED_2);
				LedOff(LED_3);
			};
			if(20<=valor_distancia && valor_distancia<30)
			{
				GPIOOn(LED_RGB_B);
				LedOn(LED_1);
				LedOn(LED_2);
				LedOff(LED_3);
			};
			if(30<=valor_distancia && valor_distancia<40)
			{
				GPIOOn(LED_RGB_B);
				LedOn(LED_1);
				LedOn(LED_2);
				LedOn(LED_3);
			};
			if(40<=valor_distancia && valor_distancia<60)
			{
				GPIOOn(LED_RGB_R);
				LedOn(LED_1);
				LedOn(LED_2);
				LedOn(LED_3);
			};
};//LedDistanceControlCentimetres()

////////////////////////////////////
/*void LedDistanceControlPulgadas(distancia)
{
	//idem que cm pero en pulgadas
};*/
///////////////////////////////////
void TeclaUno(void)
{
	/* funcion para hacer interrupcion */
	prender =! prender; //bandera
};
///////////////////////////////////
void TeclaDos(void)
{
	/* funcion para hacer interrupcion */
	hold =! hold; // bandera
};
//////////////////////////////////
void do_serial()
{
	//leo el dato que me mandan
	UartReadByte(SERIAL_PORT_PC, &dat);
	//dependiendo de la variable dat (que es global)
	switch (dat)
	{
		case 'O':
			prender =! prender;
		case 'H':
			hold =! hold;
	}


};
/////////////////////////

void Tiempo()
{
	//prender es una variable global
	if(prender)
	{
		tiempo_de_distancia = HcSr04ReadDistance();
		distancia = HcSr04ReadDistanceCentimeters(tiempo_de_distancia);

		if (hold)
		{
			LedDistanceControlCentimetres(distancia);
			ITSE0803DisplayValue(distancia);

			//muestro el mismo dato pero por la compu
			//UartItoa cambio el dato a ASCII, el numero 10 es por la base
			UartSendString(SERIAL_PORT_PC, UartItoa(distancia, 10));
			UartSendString(SERIAL_PORT_PC, " cm\n\r");
		}
	}//fin primer if
		else
		{
			GPIOOff(LED_RGB_G);
			LedOff(LED_1);
			LedOff(LED_2);
			LedOff(LED_3);
			ITSE0803DisplayValue(0);
		}//fin else

};//fin funcion Tiempo()

////////////////////////////////

int main(void)
{
	SystemClockInit();	//inicial el clock de la CIAA
	LedsInit();
	SwitchesInit();

	//Display
	gpio_t pins_configs[7] = {GPIO_LCD_1, GPIO_LCD_2, GPIO_LCD_3, GPIO_LCD_4, GPIO_1, GPIO_3, GPIO_5};
	//Puntero a vector
	gpio_t *ptr_pins_configs;
	ptr_pins_configs = pins_configs;
	ITSE0803Init(ptr_pins_configs);

	//Declaro las funciones
	HcSr04Init(GPIO_T_FIL2 , GPIO_T_FIL3);
	HcSr04ReadDistance();

	//interrupciones
	SwitchActivInt(SWITCH_1,&TeclaUno);//un puntero a funcion q creo arriba
	SwitchActivInt(SWITCH_2, &TeclaDos);// un puntero a funcion q creo arriba

	timer_config my_timer = {TIMER_A,1000,&Tiempo}; //nombre provisorio de funcion
	//1000 ms es el tiempo en q se va a ejecutar lo q esta apuntando &Tiempo
	TimerInit(&my_timer);//inicializa my_timer
	TimerStart(TIMER_A);// empieza TIMER_A

	//cargo un estruct
	//115200 es la velocidad con q se transmite los datos
	serial_config my_serial = {SERIAL_PORT_PC, 115200, &do_serial};
	//inicializo los puerto series
	UartInit(&my_serial);

	while (1)
	{

	}
	return 0;
}//fin main

/*==================[end of file]============================================*/

