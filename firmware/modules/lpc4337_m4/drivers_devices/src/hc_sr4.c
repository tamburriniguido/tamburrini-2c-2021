/*
 * hc_sr4.c
 *
 *  Created on: 3 sep. 2021
 *      Author: Guido
 */
/*==================[inclusions]=============================================*/
#include "led.h"
#include "gpio.h"
#include "switch.h"

/*==================[macros and definitions]=================================*/
/*==================[internal data declaration]==============================*/
/*==================[internal functions declaration]=========================*/

int16_t HcSr04ReadDistanceInches(void);

bool HcSr04Init(gpio_t echo, gpio_t trigger);
bool HcSr04Deinit(gpio_t echo, gpio_t trigger);
int16_t HcSr04ReadDistance(void);
int16_t HcSr04ReadDistanceCentimeters(int16_t time_for_distance);
int16_t HcSr04ReadDistancePulgadas(int16_t time_for_distance);

/*==================[internal data definition]===============================*/
/*==================[external data definition]===============================*/
/*==================[internal functions definition]==========================*/
/*==================[external functions definition]==========================*/

gpio_t echo_global;
gpio_t trigger_global;
uint16_t N_cm = 40;
uint16_t N_pulg = 129;

//////////////////////////////////////////

bool HcSr04Init(gpio_t echo, gpio_t trigger)
{
	//Configuro e inicializo los pines de entrada y salida

	GPIOInit(echo, GPIO_INPUT);//configuro cual es el pin de entrada
	GPIOInit(trigger, GPIO_OUTPUT);//configuro cual es el pin de salida

	echo_global = echo;//guardo la variable en una variable global
	trigger_global = trigger;//guardo la variable en una variable global

	return true;
}

///////////////////////////////////////////////////////

bool HcSr04Deinit(gpio_t echo, gpio_t trigger)
{
	// Borro la inicializacion de los pines que se hizo en: HcSr04Init(gpio_t echo, gpio_t trigger)
	GPIOOff(echo);
	GPIOOff(trigger);

	return true;
}

/////////////////////////////////////////////////////

int16_t HcSr04ReadDistance(void)
{
/*esta funcion calcula el tiempo de distancia hacia el objeto
con ese tiempo despues hago el calculo la distancia*/

	int16_t time_for_distance = 0;

	// mando un pulso al trigger de 10us.
	// Te lo pide la hoja de datos.
	GPIOOn(trigger_global);
	DelayUs(10);
	GPIOOff(trigger_global);

	while(GPIORead(echo_global) == 0)
	{
		//miestras "GPIORead(echo_global)" me retorne False
		//hacer "nada"
	}

	while(GPIORead(echo_global))
	{
		//cuando GPIORead(echo_global) sea True, me cuenta el tiempo
		//que tarda en ir y venir la señal, gracias a  DelayUs(1).

		time_for_distance = time_for_distance + 1;
		DelayUs(1);
	}

	return (time_for_distance);
}

///////////////////////////////////////////////////////

int16_t HcSr04ReadDistanceCentimeters(int16_t time_for_distance)
{
	//con el tiempo de distancia hacia el objeto, hago el
	//calculo y saco la distancia en cm
	int16_t distance_centimeters = 0;

	distance_centimeters = time_for_distance / N_cm;//cambiar el numero

	return (distance_centimeters);
}

//////////////////////////////////////////////////////

int16_t HcSr04ReadDistancePulgadas(int16_t time_for_distance)
{
	//con el tiempo de distancia hacia el objeto, hago el
	//calculo y saco la distancia en pulgadas
	int16_t distance_pulgadas = 0;

	distance_pulgadas = distance_pulgadas / N_pulg;//cambiar el numero

	return (distance_pulgadas);
}

/////////////////////////////////////////////////////



/*==================[end of file]============================================*/

