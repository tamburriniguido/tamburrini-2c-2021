/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor: Guido
 */

/*==================[Consigna]===============================================*/
/*Declare una estructura “alumno”, con los campos “nombre” de 12 caracteres,
 *“apellido” de 20 caracteres y edad. Defina una variable con esa estructura
 *y cargue los campos con sus propios datos.Defina un puntero a esa
 *estructura y cargue los campos con los datos de su compañero
 *(usando acceso por punteros).
 */
/*===================[NOTA]========================================*/
    /* NOMBRE DE LAS FUNCIONES ------- MayusculasSinEspacio(){}
       NOMBRE DEL #DEFINE-------------- TODO_MAYUSCULA_SEPARADO_ASI
       NOMBRE DE LAS VARIABLES-------- todo_minuscula*/
/*==================[Librerias]=============================================*/
#include "main.h"
#include <stdio.h>

/*==================[Macros and definitions]=================================*/
typedef struct
{
	char nombre[12];
	char apellido[20];
	uint8_t edad;
}alumno;

/*==============[Internal functions declaration]==================*/

int main(void)
{
	alumno individuo_1, *individuo_2;
	individuo_2 = & individuo_1;
	alumno mis_datos;

	strcpy(mis_datos.apellido,"Tamburrini");
	strcpy(mis_datos.nombre,"Guido");
	mis_datos.edad = 30;

	strcpy(individuo_2->apellido,"Cabra");
	strcpy(individuo_2->nombre,"Norberto");
	individuo_2->edad = 26;


//	preguntar xq no me tira los nombres
	printf("Apellido %d \r", mis_datos.apellido);
	printf("Apellido %d", mis_datos.nombre);

	return 0;

}

/*==================[End of file]============================================*/

