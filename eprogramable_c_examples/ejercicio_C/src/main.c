/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor: Guido
 */

/*==================[Consigna]===============================================*/
/* Escriba una función que reciba un dato de 32 bits,
 * la cantidad de dígitos de salida y un puntero a un arreglo
 * donde se almacene los n dígitos. La función deberá convertir
 * el dato recibido a BCD, guardando cada uno de los dígitos de salida
 * en el arreglo pasado como puntero.
int8_t  BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number )
{

}
*/
/*==============================[NOTA]========================================*/

    // NOMBRE DE LAS FUNCIONES ------- MayusculasSinEspacio(){}

    // NOMBRE DEL DEFINE-------------- TODO_MAYUSCULA_SEPARADO_ASI

    // NOMBRE DE LAS VARIABLES-------- todo_minuscula

/*==================[Librerias]=============================================*/
#include "main.h"
#include <stdio.h>

/*==================[Macros and definitions]=================================*/


/*==================[Internal functions declaration]=========================*/

int8_t BinaryToBcd(uint32_t data, uint8_t digits, uint8_t *bcd_number )
{
	uint8_t i;
	uint8_t resto= 0;
	uint8_t resultado = 0;

	for (i=0; i<digits; i++)
	{
		resto = data % 10;
		data = data / 10;
		bcd_number[i] = resto;

		printf (" %d \r\n", bcd_number[i]);
	}
return 0;
};

int main(void)
{
	uint32_t dato = 0;
	uint8_t digitos = 0; //sera de 8 o 16??

	uint8_t vector_datos[10];
	uint8_t *ptr_dato;
	ptr_dato = vector_datos;

	dato = 245;
	printf ("Ingrese el dato %d \r\n", dato);
	digitos = 3;
	printf ("Ingrese el numero de digitos  %d \r\n", digitos);


	BinaryToBcd(dato, digitos, ptr_dato);

	return 0;
}

/*==================[End of file]============================================*/

