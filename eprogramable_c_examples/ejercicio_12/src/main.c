/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor: Guido
 */

/*==================[Consigna]===============================================*/
/*Declare un puntero a un entero con signo de 16 bits y cargue
 *inicialmente el valor -1. Luego, mediante máscaras, coloque un 0
 *en el bit 4
 */
/*==============================[NOTA]================================*/
    /* NOMBRE DE LAS FUNCIONES ------- MayusculasSinEspacio(){}
       NOMBRE DEL #DEFINE-------------- TODO_MAYUSCULA_SEPARADO_ASI
       NOMBRE DE LAS VARIABLES-------- todo_minuscula*/
/*==================[Librerias]=============================================*/
#include "main.h"
#include <stdio.h>

/*==================[Macros and definitions]=================================*/
#define BIT_4 4
#define BIT_0 0

/*==================[Internal functions declaration]=========================*/

int main(void)
{
	uint32_t MASK;
	int16_t *ptr;
	uint32_t resultado;

    *ptr = (-1<<BIT_0);
    MASK = ~(1<<BIT_4);
    resultado = *ptr & MASK;

    printf(" %d ", resultado);

	return 0;
}

/*==================[End of file]============================================*/

