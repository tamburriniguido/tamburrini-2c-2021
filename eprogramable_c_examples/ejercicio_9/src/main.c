/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor: Guido
 */

/*==================[Consigna]===============================================*/
/*Sobre una constante de 32 bits previamente declarada, verifique si
 *el bit 4 es 0. Si es 0, cargue una variable “A” previamente declarada
 *en 0, si es 1, cargue “A” con 0xaa. Para la resolución de la lógica,
 *siga el diagrama de flujo siguiente:
 *
 */
/*==============================[NOTA]========================================*/

    // NOMBRE DE LAS FUNCIONES ------- MayusculasSinEspacio(){}

    // NOMBRE DEL #DEFINE-------------- TODO_MAYUSCULA_SEPARADO_ASI

    // NOMBRE DE LAS VARIABLES-------- todo_minuscula

/*==================[Librerias]=============================================*/
#include "main.h"
#include <stdio.h>

/*==================[Macros and definitions]=================================*/
#define BIT4 4
#define BIT0 0
const uint32_t CONST;
uint32_t A;
/*==================[Internal functions declaration]=========================*/

int main(void)
{
	if (CONST & BIT4){A = '0xaa';}

	else {A = (0<<BIT0);}

	printf (" %d ", A);

	return 0;
}

/*==================[End of file]============================================*/

