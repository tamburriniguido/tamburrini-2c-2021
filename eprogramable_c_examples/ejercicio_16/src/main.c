/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor: Guido
 */

/*==================[consigna]===============================================*/
/*Declare una variable sin signo de 32 bits y cargue el valor 0x01020304.
Declare cuatro variables sin signo de 8 bits y, utilizando máscaras,
rotaciones y truncamiento, cargue cada uno de los bytes de la variable
de 32 bits.Realice el mismo ejercicio, utilizando la definición de una
 “union”.
*/
/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
/*==================[macros and definitions]=================================*/
uint32_t VARIABLE = 0x01020304;
uint8_t a;
uint8_t b;
uint8_t c;
uint8_t d;

uint8_t VARIABLE_0b=0;
uint8_t VARIABLE_1b=1;
uint8_t VARIABLE_2b=2;
uint8_t VARIABLE_3b=3;

/*==================[internal functions declaration]=========================*/

int main(void)
{
	a = VARIABLE;
	b = (VARIABLE>>8);
	c = (VARIABLE>>16);
	d = (VARIABLE>>24);

	printf(" %d",a);
	printf(" %d",b);
	printf(" %d",c);
	printf(" %d",d);


	return 0;

}


/*==================[end of file]============================================*/

