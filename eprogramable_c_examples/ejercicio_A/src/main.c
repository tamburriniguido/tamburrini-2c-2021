/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor: Guido
 */

/*==================[consigna]===============================================*/
/*
 * Realice un función que reciba un puntero a una estructura LED como la que se
 * muestra a continuación:

struct leds
{
	uint8_t n_led;      indica el número de led a controlar
	uint8_t n_ciclos;   indica la cantidad de ciclos de encendido/apagado
	uint8_t periodo;    indica el tiempo de cada ciclo
	uint8_t modo;       ON, OFF, TOGGLE
} my_leds;
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <synchapi.h> // libreria de funcion sleep

/*==================[macros and definitions]=================================*/
#define OFF 0
#define ON 1
#define TOGGLE 2

typedef struct
{
	uint8_t n_led;      // indica el número de led a controlar
	uint8_t n_ciclos;   // indica la cantidad de ciclos de encendido/apagado
	uint8_t periodo;    // indica el tiempo de cada ciclo
	uint8_t modo;       // OFF 0, ON 1, TOGGLE 2
} my_leds;

/*==================[internal functions declaration]=========================*/

void PrenderLed (uint8_t led)
{
	printf ("Se encendio el led %d \r\n",led);
};

void ApagarLed (uint8_t led)
{
	printf ("Se apago el led %d \r\n", led);
};

void RetardoLed (uint8_t periodo) // El periodo esta en... unidad?
{
	Sleep (periodo);
};

void ToggleLed (uint8_t led, uint8_t ciclos, uint8_t periodo)
{
	uint8_t ciclo_minimos = 0;

	while (ciclo_minimos < ciclos)
	{
		PrenderLed (led);
		RetardoLed (periodo);
		ApagarLed(led);
		RetardoLed (periodo);

		ciclo_minimos ++;
	}
};

void LedControl(my_leds *led_puntero)
{
	switch (led_puntero -> modo)
	{
		case 0:
			ApagarLed (led_puntero -> n_led);
			break;

		case 1:
			ApagarLed (led_puntero -> n_led);
			break;

		case 2:
			ToggleLed (led_puntero -> n_led, led_puntero -> n_ciclos, led_puntero -> periodo);
			break;
	}
};


int main(void)
{
	my_leds led_estruct , *led_ptr;
	led_ptr = &led_estruct;

	//cargo el struct con valores definidos para probar
	led_estruct.modo = 1;
	led_estruct.n_ciclos = 4;
	led_estruct.n_led = 3;
	led_estruct.periodo = 6;


	LedControl(led_ptr);

	return 0;
}

/*==================[end of file]============================================*/

