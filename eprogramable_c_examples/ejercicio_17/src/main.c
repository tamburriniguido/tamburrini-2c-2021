/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor: Guido
 * 
 */
/*==================[consigna]==============================================*/
/*
Realice un programa que calcule el promedio de los 15 números listados abajo,
para ello, primero realice un diagrama de flujo similar al presentado en el ejercicio 9.
(Puede utilizar la aplicación Draw.io). Para la implementación, utilice el menor tamaño
de datos posible:
*/
#include <stdio.h>
#include <stdint.h>
/*==================[macros and definitions]=================================*/
const uint8_t DATOS[] = {234,123,111,101,32,116,211,24,214,100,124,222,1,129,9};
 uint16_t SUMATORIA = 0;
 uint8_t DIVIDENDO = 15;
 uint8_t RESULTADO;
 uint8_t i;

/*==================[internal functions declaration]=========================*/

int main(void)
{
   for (i=0; i<15; i++)
   {
	   SUMATORIA = DATOS[i] + SUMATORIA;
   }
RESULTADO = SUMATORIA/DIVIDENDO;
printf("%d\r",RESULTADO);
return 0;
}
