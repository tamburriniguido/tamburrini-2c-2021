/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor: Guido
 */

/*==================[Consigna]===============================================*/
/*Sobre una variable de 32 bits sin signo previamente declarada y de valor
 *desconocido, asegúrese de colocar el bit 3 a 1 y los bits 13 y 14 a 0
 *mediante máscaras y el operador <<.
 *
 */
/*==============================[NOTA]========================================*/

    // NOMBRE DE LAS FUNCIONES ------- MayusculasSinEspacio(){}

    // NOMBRE DEL #DEFINE-------------- TODO_MAYUSCULA_SEPARADO_ASI

    // NOMBRE DE LAS VARIABLES-------- todo_minuscula

/*==================[Librerias]=============================================*/
#include "main.h"
#include <stdio.h>

/*==================[Macros and definitions]=================================*/
#define BIT_3 3
#define BIT_13 13
#define BIT_14 14

/*==================[Internal functions declaration]=========================*/

int main(void)
{
    uint32_t variable;
    uint32_t resultado;
    uint32_t mask;

    mask = (1<<BIT_3)& ~(1<<BIT_13)& ~(1<<BIT_14);
    resultado = variable & mask;
    printf (" %d ", resultado);
	return 0;
}

/*==================[End of file]============================================*/

