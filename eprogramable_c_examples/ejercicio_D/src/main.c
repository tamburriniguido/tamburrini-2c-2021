/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2021
 * Autor: Guido
 */

/*==================[Consigna]===============================================*/

/*Escribir una función que reciba como parámetro un dígito BCD y
  un vector de estructuras del tipo  gpioConf_t.

typedef struct
{
	uint8_t port;				!< GPIO port number
	uint8_t pin;				!< GPIO pin number
	uint8_t dir;				!< GPIO direction ‘0’ IN;  ‘1’ OUT
} gpioConf_t;

Defina un vector que mapee los bits de la siguiente manera:
b0 -> puerto 1.4
b1 -> puerto 1.5
b2 -> puerto 1.6
b3 -> puerto 2.14

La función deberá establecer en qué valor colocar cada
bit del dígito BCD e indexando el vector anterior
operar sobre el puerto y pin que corresponda.
 */
/*==============================[NOTA]========================================*/

    // NOMBRE DE LAS FUNCIONES ------- MayusculasSinEspacio(){}

    // NOMBRE DEL #DEFINE-------------- TODO_MAYUSCULA_SEPARADO_ASI

    // NOMBRE DE LAS VARIABLES-------- todo_minuscula

/*==================[Librerias]=============================================*/
#include "main.h"
#include <stdio.h>

/*==================[Macros and definitions]=================================*/
#define IN 0
#define OUT 1
/*==================[Internal functions declaration]=========================*/

typedef struct
{
	uint8_t port;				//!< GPIO port number
	uint8_t pin;				//!< GPIO pin number
	uint8_t dir;				//!< GPIO direction ‘0’ IN;  ‘1’ OUT
}gpioConf_t;

void ControlPuertos(uint8_t bcd, gpioConf_t *puerto_ptr)
{
	uint8_t i=0;
	for(i=0; i<4; i++)
	{
		printf("El puerto %d ",puerto_ptr->port, puerto_ptr->pin);

		if(bcd % 2 == 1){printf("Puerto en alto \r\n");}
			else{printf("Puerto en bajo \r\n");}

		bcd = bcd/2;
		puerto_ptr++;
	}
}

int main(void)
{
	uint8_t bcd;

	//puertos es un vectos con 4 elementos.
   	gpioConf_t puertos[4],*puerto_ptr;

	puertos[0].port = 2;
	puertos[0].pin = 8;

	puertos[1].port = 1;
	puertos[1].pin = 2;

	puertos[2].port = 2;
	puertos[2].pin = 1;

	puertos[3].port = 2;
	puertos[3].pin = 9;

	puerto_ptr = puertos;

	bcd = 3;
   	ControlPuertos(bcd, puerto_ptr);

	sleep(10);

	return 0;
}

/*==================[End of file]============================================*/

